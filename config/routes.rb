Rails.application.routes.draw do
  devise_for :users
  resources :links
  get ':slug' => 'links#show'
  root 'home#index'
end
