class HomeController < ApplicationController
  before_action :authenticate_user!, only: [:index]

  def index
    @link = Link.new
    @top_links = Link.order(clicks: :desc).first(12)
  end
end
