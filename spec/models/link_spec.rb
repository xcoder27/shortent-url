require 'rails_helper'

RSpec.describe Link, type: :model do
  it "Url validation" do
    should validate_presence_of(:given_url)
  end
end
