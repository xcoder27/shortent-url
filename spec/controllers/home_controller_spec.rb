require 'rails_helper'

RSpec.describe HomeController, type: :controller do
  
  describe "GET #index" do
    it "responds successfully with an HTTP 200 status code" do
      expect(response).to be_success
      expect(response).to have_http_status(200)
    end

    it "renders the index template" do
      get :index, params: { user_id: 1 }
      expect(response).to_not render_template("home/index")
    end
  end
end